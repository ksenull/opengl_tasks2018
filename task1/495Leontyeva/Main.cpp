#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>
#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include "Main.h"

class Task1Application : public Application {
    MeshPtr _surface;
    ShaderProgramPtr _shader;
    unsigned _detalization;;

    void makeScene() override
    {
        Application::makeScene();

       // _cameraMover = std::make_shared<FreeCameraMover>();
        _detalization = DEFAULT_DETALIZATION_N;
        //Создаем меш
        _surface = makePseudosphere(0.5f, _detalization);
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("495LeontyevaData/shaderNormal.vert", "495LeontyevaData/shader.frag");
    }

    void update() override
    {
        Application::update();

        //Вращаем
        float angle = static_cast<float>(glfwGetTime());

        glm::mat4 mat;
        mat = glm::translate(mat, glm::vec3(0.0f, 0.5f, 0.0));
        mat = glm::rotate(mat, angle, glm::vec3(0.0f, 0.0f, 1.0f));

        _surface->setModelMatrix(mat);
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем меш
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _surface->draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_ESCAPE)
            {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            }
            if (key == GLFW_KEY_EQUAL)
            {
                _detalization += DETALIZATION_STEP;
                //std::cout << "Detalization + 20\n";
                _surface = makePseudosphere(0.5f, _detalization);
                _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
            }
            if (key == GLFW_KEY_MINUS) {
                _detalization -= DETALIZATION_STEP;
                _surface = makePseudosphere(0.5f, _detalization);
                _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
            }
        }

        _cameraMover->handleKey(_window, key, scancode, action, mods);
    }
};

int main(int argc, char** argv)
{
    Task1Application app;
    app.start();

    return 0;
}